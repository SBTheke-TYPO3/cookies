<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cookies\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class CookiesUtility {

    public const ONE_DAY = 60 * 60 * 24;
    public const ONE_YEAR = 60 * 60 * 24 * 365;

    /**
     * Delete all cookies
     *
     * @param bool $onlyExt Delete only cookies set by EXT:cookies (prefixed with "tx_cookies-")
     * @return void
     */
    public static function deleteAll(bool $onlyExt = false): void
    {
        if (!empty($_COOKIE)) {
            $domain = GeneralUtility::getIndpEnv('HTTP_HOST');
            $domainParts = GeneralUtility::trimExplode('.', $domain);
            foreach ($_COOKIE as $cookie => $cookieValue) {
                if (!$onlyExt || str_starts_with($cookie, 'tx_cookies-')) {
                    $domainTmp = $domain;
                    setcookie($cookie, false, time() - self::ONE_YEAR);
                    setcookie($cookie, false, time() - self::ONE_YEAR, '/');
                    for ($i = 0; $i <= count($domainParts) - 2; $i++) {
                        setcookie($cookie, false, time() - self::ONE_YEAR, '/', $domainTmp);
                        $domainTmp = ltrim(ltrim($domainTmp, '.'), $domainParts[$i]); // remove leading subdomain
                    }
                }
            }
        }
    }
}
