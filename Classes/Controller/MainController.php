<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cookies\Controller;

use Psr\Http\Message\ResponseInterface;
use SBTheke\Cookies\Utility\CookiesUtility;

class MainController extends AbstractController {

    /**
     * Action cookie: display info to accept cookies
     *
     * @param string $redirect Hand over redirect url.
     * @return ResponseInterface
     * @noinspection PhpUnused Used for plugin
     */
    public function cookieAction(string $redirect = ''): ResponseInterface
    {
        if (!empty($_COOKIE['tx_cookies-accepted']) && !$this->settings['showPermanent']) {
            return $this->htmlResponse(''); // Do not render this action
        }
        $this->view->assign('cookies', $_COOKIE);

        $selectionMade = false;
        foreach ($_COOKIE as $cookie => $cookieValue) {
            if (str_starts_with($cookie, 'tx_cookies-')) {
                $selectionMade = true;
                break;
            }
        }
        $this->view->assign('selectionMade', $selectionMade);
        $this->view->assign('redirect', $redirect);

        return $this->htmlResponse();
    }

    /**
     * Action cookie (cached): display info to accept cookies
     *
     * @return ResponseInterface
     * @noinspection PhpUnused Used for plugin
     */
    public function cookieCachedAction(): ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * Action submit: Process user selection
     *
     * @param bool|null $disable Is "true", if user denies cookies. Else it's "null".
     * @param bool|null $accept Is "true", if user accepts cookies. Else it's "null".
     * @param string $redirect Redirect to this url after processing.
     * @return ResponseInterface
     * @noinspection PhpUnused Used for plugin
     */
    public function submitAction(bool $disable = null, bool $accept = null, string $redirect = ''): ResponseInterface
    {
        CookiesUtility::deleteAll(true);
        $expire = time() + (CookiesUtility::ONE_DAY * $this->settings['expire']);
        setcookie('tx_cookies-consent', 1, $expire, '/');
        if ($disable === true) {
            CookiesUtility::deleteAll();
            setcookie('tx_cookies-disabled', 1, $expire, '/');
            $this->addFlashMessage(
                $this->translate('flashmessage.disabled'),
                $this->translate('flashmessage.disabled.title')
            );
        }
        if ($accept === true) {
            setcookie('tx_cookies-accepted', 1, $expire, '/');
            $this->addFlashMessage(
                $this->translate('flashmessage.enabled'),
                $this->translate('flashmessage.enabled.title')
            );
        }
        if (!empty($types = $this->settings['types'])) {
            $arguments = $this->request->getArguments();
            foreach ($types as $k => $v) {
                if (array_key_exists('type-' . $k, $arguments) && $arguments['type-' . $k]) {
                    setcookie('tx_cookies-type-' . $k, 1, $expire, '/');
                }
            }
        }
        if ($redirect) {
            return $this->redirectToUri($redirect, null, 302);
        } else {
            return $this->redirect('cookie');
        }
    }
}
