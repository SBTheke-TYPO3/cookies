<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cookies\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

abstract class AbstractController extends ActionController {

    /**
     * Helper function to use localized strings
     *
     * @param string $key locallang key
     * @param string $defaultMessage the default message to show if key was not found
     * @return string
     */
    protected function translate(string $key, string $defaultMessage = ''): string
    {
        $message = LocalizationUtility::translate($key, 'cookies');
        if ($message === null) {
            $message = $defaultMessage;
        }
        return $message;
    }

}
