<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SBTheke\Cookies\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SBTheke\Cookies\Utility\HelperUtility;
use Symfony\Component\HttpFoundation\Response;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Core\Context\Context;

class ConsentRedirect implements MiddlewareInterface
{
    /**
     * Check consent and redirect to specific page if no consent was given.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws AspectNotFoundException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $pageIdConsent = (int)@$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_cookies.']['settings.']['pidRedirect'];
        if ($pageIdConsent) {

            // Do not redirect when consent given
            if (empty($_COOKIE['tx_cookies-consent'])) {
                // Do not redirect search bots
                if (!HelperUtility::isBotDetected()) {
                    // Do not redirect when on consent page
                    if ((int)$GLOBALS['TSFE']->id !== $pageIdConsent) {
                        $frontendUserLoggedIn = GeneralUtility::makeInstance(Context::class)->getPropertyFromAspect('frontend.user', 'isLoggedIn');
                        // Do not redirect logged-in users
                        if (!$frontendUserLoggedIn) {
                            // Redirect
                            /** @var UriBuilder $uriBuilder */
                            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
                            return new RedirectResponse(
                                $uriBuilder
                                    ->reset()
                                    ->setTargetPageUid($pageIdConsent)
                                    ->uriFor(null, ['redirect' => $request->getUri()->getPath()], 'Main', 'cookies', 'Pi1'),
                                Response::HTTP_PRECONDITION_FAILED
                            );
                        }
                    }
                }
            }

            // Redirect if consent page is requested but consent already given
            if ((int)$GLOBALS['TSFE']->id === $pageIdConsent) {
                if ($_COOKIE['tx_cookies-accepted']) {
                    $redirect = $this->getRedirectParamFromRequest($request);
                    if (!$redirect) {
                        // Redirect to start page
                        $redirect = '/';
                    }
                    return new RedirectResponse($redirect, Response::HTTP_FOUND);
                }
            }

            // Redirect if parameter exists, but not on consent page
            if ((int)$GLOBALS['TSFE']->id !== $pageIdConsent) {
                if ($redirect = $this->getRedirectParamFromRequest($request)) {
                    return new RedirectResponse($redirect, Response::HTTP_FOUND);
                }
            }
        }

        return $handler->handle($request);
    }

    /**
     * Get parameter "redirect" from request, if any.
     *
     * @param ServerRequestInterface $request
     * @return string
     */
    protected function getRedirectParamFromRequest(ServerRequestInterface $request): string
    {
        $routeResult = $request->getAttribute('routing');
        if (
            $routeResult instanceof PageArguments &&
            is_array($routeResult->getQueryArguments()) &&
            !empty($redirect = $routeResult->getQueryArguments()['redirect'])
        ) {
            return $redirect;
        }
        return '';
    }
}
