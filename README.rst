=============
Documentation
=============

----------------
What does it do?
----------------

- Show a hint that cookies are used on your website.
- Hint can be closed or permanently hidden.
- "Read more" link (links to a page where the visitor can find more informations about cookies).
- Visitor has the possibility to disable cookies.
- Visitor has the possibility to enable different types of cookies (e.g. essential, marketing, analytics).
- Displays a small button for showing the cookie consent again.
- Visitor can be redirected to a specific page for consent (so complete website can be blocked until visitor gave his consent). Search bots/crawlers aren't redirected.
- Widget can be added as plugin in one page or with TypoScript on every page.
- Provides a cached plugin.
- Highly configurable: All features can be enabled or disabled in TypoScript, all texts can be overwritten.
- Fluid template. You can modify anything!
- Based on Bootstrap framework. But also works without.
- Uses JavaScript/jQuery to set cookie (so no page reload is necessary).
- Fallback for browsers with disabled JavaScript.

What does it not do?
====================

It doesn't provide an easy way to show a cookie hint. If you want an easy way, you should have a look at https://www.osano.com/cookieconsent or https://www.cookiebot.com/en/cookie-banner/.
Use this extension if you want a flexible solution (you have different settings and you can overwrite HTML and JavaScript), without loading third-party scripts and with the possibility to disable cookies.


-----------
Screenshots
-----------

.. figure:: Documentation/screenshot1.png

    Simple cookie consent.


.. figure:: Documentation/screenshot2.png

    Cookie consent with possibility to disable cookies.


.. figure:: Documentation/screenshot3.png

    Cookie consent with possibility to give consent for different categories.


.. figure:: Documentation/screenshot4.png

    Plugin settings.


-----
Hints
-----

If you have a login on your page, the login is possible because **this extension does not prevent the creation of new cookies**. In this case, you should write a hint that a cookie is created when logging in.
If you use analytic tools or other extensions, which create cookies, these cookies are still created if you do not prevent his. See chapter "TypoScript condition" for details how you can prevent this.


------------
Installation
------------

After installing this extension you have to add the template "Cookie Control (cookies)" in field "Include static (from extensions)".
Don't forget to include jQuery library (not provided by this extension)!
Add the plugin "Cookies" as content element to a page (e.g. the start page)...

...or on every page with Fluid...

::

    <f:cObject typoscriptObjectPath="tt_content.list.20.cookies_pi1">Cookie Hint</f:cObject>


...or with TypoScript:

::

    page.5 < tt_content.list.20.cookies_pi1


-------------
Configuration
-------------


TypoScript constants
====================

Your configuration options:

.. figure:: Documentation/screenshot5.png

    Configuration in constant editor.


TypoScript setup
================

Configuration of different consent types:

::

    plugin.tx_cookies.settings.types {
        essential = 2
        marketing = 0
        analytics = 1
    }


The value is used for the initial status of the checkbox:

- 0 = unchecked
- 1 = checked
- 2 = checked and disabled


Change the text which is shown in the widget
============================================

You can change the text in TypoScript:

::

    plugin.tx_cookies._LOCAL_LANG.en {
        cookie.close = Your text
        cookie.heading = Your text
    }


Disabling cookies
=================

If you provide the possibility to disable cookies, it's highly recommended to create a new page and set the page ID in TypoScript constant "pidProcess".
On this page, the disabling of cookies is processed. Per default, a redirect to the first displayed page occurs, but you can change this by removing the hidden redirect input field from the Fluid template or partial "Redirect".


--------------------
TypoScript condition
--------------------

If the visitor didn't accept cookies, you should deactivate analytic tools like Google Analytics or Piwik, because these tools create new cookies. This is possible with a TypoScript condition:

::

    page.10 = TEXT
    page.10.value = [Google Analytics Tracking Code]
    [traverse(request.getCookieParams(), 'tx_cookies-disabled')]
        page.10 >
    [END]

To enable cookies only after the visitor has accepted them, you have to disable all analytics tools and other stuff until there's the cookie "tx_cookies-accepted":

::

    page.10 = TEXT
    [traverse(request.getCookieParams(), 'tx_cookies-accepted') || traverse(request.getCookieParams(), 'tx_cookies-type-tracking')]
        page.10.value = [Google Analytics Tracking Code]
    [END]


Cached version of plugin
========================

By default, the cookie plugin is uncached (USER_INT). That is necessary to not render the plugin if the visitor has already closed the cookie hint.
Sometimes, it might be better to use a cached plugin, e.g. for performance reasons or when using extensions like "staticfilecache".
You have the possibility to use the cached plugin by adding this plugin as content element or with TypoScript:

::

    page.5 < tt_content.list.20.cookies_pi2

But then, you have to show the cookie hint by your own, when the visitor didn't made a choice yet, e.g. with JavaScript:

::

    /**
     * @param {string} s name (or part of name) of cookie
     * @return {boolean}
     */
    function getCookieValue(s) {
        const b = document.cookie.match('(^|;)\\s*' + s + '([^;]+)\\s*=\\s*([^;]+)');
        return !!b;
    }
    if (!getCookieValue('tx_cookies-')) {
        document.getElementById('tx_cookies_inner').classList.add('show');
        document.getElementById('tx_cookies_showPermanent').classList.remove('show');
    }


Show cookie widget only in specific countries/languages
=======================================================

Use this TypoScript condition:

::

    [siteLanguage("locale") == "de_DE"]
        page.5 < tt_content.list.20.cookies_pi1
    [END]


Redirect to consent page
========================

You are able to block the complete website until the visitor gave his consent. Please make sure this complies with the currently applicable laws of the country.

Use TS constant "pidRedirect" for that: If set, the visitor is redirected to this page where he can give his consent.
You should also:

- Unset "pidRedirect" on pages which shouldn't be blocked (e.g. imprint or data privacy page).
- Adapt the Fluid template to your needs (e.g. remove the close button).
- Eventually include the cookies plugin only on the consent page (advantage is, that plugin don't need to be rendered), e.g. by adding the plugin as content element or in TypoScript:

::

    [getTSFE().id == {$plugin.tx_cookies.settings.pidRedirect}]
        page.5 < tt_content.list.20.cookies_pi1
    [END]


----------
To-Do list
----------

You have ideas? Contact me!


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
