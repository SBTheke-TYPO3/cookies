<?php

return [
    'frontend' => [
        'sbtheke/cookies/consent-redirect' => [
            'target' => \SBTheke\Cookies\Middleware\ConsentRedirect::class,
            // After tsfe middleware, else TypoScript is not available
            'after' => [
                'typo3/cms-frontend/tsfe'
            ],
        ],
    ],
];
