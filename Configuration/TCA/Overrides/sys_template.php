<?php
defined('TYPO3') || defined('TYPO3_MODE') || die('Access denied');

(function($table) {

    // TypoScript template
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cookies', 'Configuration/TypoScript', 'Cookie Control');

})('sys_template');
