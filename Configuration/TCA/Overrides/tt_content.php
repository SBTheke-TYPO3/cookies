<?php
defined('TYPO3') || defined('TYPO3_MODE') || die('Access denied');

(function($table) {

    // Plugins
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'cookies',
        'Pi1',
        'LLL:EXT:cookies/Resources/Private/Language/locallang_be.xlf:plugin.pi1.title'
    );
    $GLOBALS['TCA'][$table]['types']['list']['subtypes_addlist']['cookies_pi1'] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('cookies_pi1', 'FILE:EXT:cookies/Configuration/FlexForms/flexform.xml');

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'cookies',
        'Pi2',
        'LLL:EXT:cookies/Resources/Private/Language/locallang_be.xlf:plugin.pi2.title'
    );
    $GLOBALS['TCA'][$table]['types']['list']['subtypes_addlist']['cookies_pi2'] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('cookies_pi2', 'FILE:EXT:cookies/Configuration/FlexForms/flexform.xml');

    // Remove some fields from the plugin
    $GLOBALS['TCA'][$table]['types']['list']['subtypes_excludelist']['cookies_pi1'] = 'layout,select_key,pages,recursive';
    $GLOBALS['TCA'][$table]['types']['list']['subtypes_excludelist']['cookies_pi2'] = 'layout,select_key,pages,recursive';

})('tt_content');
