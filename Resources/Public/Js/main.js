$(function() {
    // Accept all cookies
    $('#tx_cookies_accept').submit(function(e) {
        e.preventDefault();
        deleteAllTxCookies();

        // Set cookie
        const d = new Date();
        d.setTime(d.getTime() + ($(this).attr('data-expire')*24*60*60*1000));
        const expires = 'expires=' + d.toUTCString();
        document.cookie = 'tx_cookies-accepted=1;' + expires + ';path=/';
        document.cookie = 'tx_cookies-consent=1;' + expires + ';path=/';

        // Close cookie hint or redirect
        const redirect = $(this).find('[name="tx_cookies_pi1[redirect]"]').val()
        if (redirect) {
            window.location.replace(redirect);
        } else {
            if ($('#tx_cookies_close').length) {
                $('#tx_cookies_close').click();
            } else {
                $('#tx_cookies_inner').hide();
            }
        }
    });

    // Accept types of cookies
    $('#tx_cookies_types').submit(function(e) {
        e.preventDefault();
        deleteAllTxCookies();

        // Set cookie
        const d = new Date();
        d.setTime(d.getTime() + ($(this).attr('data-expire')*24*60*60*1000));
        const expires = 'expires=' + d.toUTCString();
        document.cookie = 'tx_cookies-consent=1;' + expires + ';path=/';

        // Create cookies for types
        $(this).find('input[type="checkbox"][name^="tx_cookies_pi1[type-"]').each(function() {
            if ($(this).is(':checked')) {
                document.cookie = $(this).attr('id') + '=1;' + expires + ';path=/';
            }
        });

        // Close cookie hint or redirect
        const redirect = $(this).find('[name="tx_cookies_pi1[redirect]"]').val()
        if (redirect) {
            window.location.replace(redirect);
        } else {
            if ($('#tx_cookies_close').length) {
                $('#tx_cookies_close').click();
            } else {
                $('#tx_cookies_inner').hide();
            }
        }
    });

    // Allow only essential cookies
    $('#tx_cookies_disable').submit(function(e) {
        e.preventDefault();
        deleteAllTxCookies();

        // Set cookie
        const d = new Date();
        d.setTime(d.getTime() + ($(this).attr('data-expire')*24*60*60*1000));
        const expires = 'expires=' + d.toUTCString();
        document.cookie = 'tx_cookies-consent=1;' + expires + ';path=/';
        document.cookie = 'tx_cookies-disabled=1;' + expires + ';path=/';

        // Close cookie hint or redirect
        const redirect = $(this).find('[name="tx_cookies_pi1[redirect]"]').val()
        if (redirect) {
            window.location.replace(redirect);
        } else {
            if ($('#tx_cookies_close').length) {
                $('#tx_cookies_close').click();
            } else {
                $('#tx_cookies_inner').hide();
            }
        }
    });
});

function deleteAllTxCookies()
{
    document.cookie.split(';').forEach(cookie => {
        // Trim leading spaces and split the cookie into name and value
        const [name] = cookie.split('=').map(c => c.trim());

        // Delete cookie
        document.cookie = name + '=;Max-Age=-99999999;path=/';
    });
}
