<?php

defined('TYPO3') || defined('TYPO3_MODE') || die('Access denied');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'cookies',
    'Pi1',
    [
        \SBTheke\Cookies\Controller\MainController::class => 'cookie, submit',
    ],
    // non-cacheable actions
    [
        \SBTheke\Cookies\Controller\MainController::class => 'cookie, submit',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'cookies',
    'Pi2',
    [
        \SBTheke\Cookies\Controller\MainController::class => 'cookieCached, submit',
    ],
    // non-cacheable actions
    [
        \SBTheke\Cookies\Controller\MainController::class => 'submit',
    ]
);
