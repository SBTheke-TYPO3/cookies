<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cookies".
 *
 * Auto generated 24-05-2024 12:22
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Cookies',
  'description' => 'Show cookie consent, optional with possibility to deactivate some or all of them.',
  'category' => 'fe',
  'version' => '9.0.5',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '11.5.0-12.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

